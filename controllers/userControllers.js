const User = require (`./../models/User`)

module.exports.register = async (reqBody) => {
    // console.log(reqBody)
    const {firstName, lastName, email, password} = reqBody
    
    const newUser = new User ({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: password
    })
    return await newUser.save().then(result => {
        if (result) {
            return true
        } else {
            if(result == null){
                return false
            }
        }
    })
}

// GET ALL USERS
module.exports.getAllUsers = async () => {
    return await User.find().then(result => result)
}