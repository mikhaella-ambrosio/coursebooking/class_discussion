const express = require(`express`);
const router = express.Router();

// Import units of functions from userController module
const {
    register,
    getAllUsers
} = require(`./../controllers/userControllers`)

// GET ALL USERS
router.get(`/`, async (req, res) => {
    try {
        await getAllUsers().then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})

// REGISTER A USER
router.post(`/register`, async (req, res) => {
    // console.log(req.body)
    
    try {
        await register(req.body).then(result => res.send(result))
    } catch(err){
        res.status(500).json(err)
    }
})



// export this module to be used in index.js
module.exports = router;