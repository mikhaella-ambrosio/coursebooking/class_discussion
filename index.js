const express = require(`express`);
const mongoose = require(`mongoose`);
const dotenv = require(`dotenv`).config();

const PORT = 3007;
const app = express();

// Connect the routes module
const userRoutes = require(`./routes/userRoutes`);

// Middlewares to handle json payloads
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Connect database to server
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

// Test DB connection
const db = mongoose.connection;
db.on("error", console.error.bind(console, 'connection error:'));
db.once("open", () => console.log(`Connected to Database`));

// Schema
    // /models/User,js

// Routes 
    // create a middleware to be the root url of all routes
app.use(`/api/users`, userRoutes);

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`));